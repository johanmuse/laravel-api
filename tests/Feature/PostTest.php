<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;

class PostTest extends TestCase
{

    public function test_bisa_bikin_post(){

        $response = $this->json('POST', '/api/posts', [
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'published_at' => date('Y-m-d H:i:s'),
        ]);

        \Log::info(1, [$response->getContent()]);

        $response->assertStatus(201);
        $response->json();

    }

    public function test_bisa_update_post(){
       
        $response = $this->json('PUT', '/api/posts/1',[
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph
        ]);

        \Log::info(1, [$response->getContent()]);

        $response->assertStatus(200);
    }
    
    public function test_bisa_show_post(){
        
        $response = $this->json('GET','api/posts/1');

        \Log::info(1, [$response->getContent()]);

        $response->assertStatus(200);
    }
    
    
    public function test_bisa_delete_post(){ 
    
        $response = $this->json('DELETE','api/posts/1');
    
        \Log::info(1, [$response->getContent()]);
    
        $response->assertStatus(200);
    
    }
    
    public function test_bisa_tampilkan_list_posts(){
    
        $response = $this->json('GET','api/posts');

        \Log::info(1, [$response->getContent()]);
        
        $response->assertStatus(200);
      
    }
}