<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'posts'], function() {
    Route::get('/', [App\Http\Controllers\PostController::class, 'index'])->name('posts');
    Route::get('/{id}', [App\Http\Controllers\PostController::class, 'show'])->name('posts.show');
    Route::post('/', [App\Http\Controllers\PostController::class, 'store'])->name('posts.store');
    Route::put('/{id}', [App\Http\Controllers\PostController::class, 'update'])->name('posts.update');
    Route::delete('/{id}', [App\Http\Controllers\PostController::class, 'delete'])->name('posts.delete');
});
