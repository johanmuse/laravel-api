<?php

namespace App\Utils;

use Illuminate\Http\JsonResponse;
use Exception;

class ApiWrapper {
    /**
     * @var string $setMessage
     */
    public static string $setMessage = '';

    /**
     * @var string $setErrors
     */
    public static $setErrors = [];

    /**
     * @var int $setStatus
     */
    public static int $setStatus = 200;

    /**
     * @var int $setData
     */
    public static $setData = [];

    /**
     * @return JsonResponse
     * @throws Exception
     */
    public static function getResponse(): JsonResponse
    {
        $result = [];

        $result['status'] = ApiWrapper::$setStatus;

        if (!empty(ApiWrapper::$setMessage)) {
            $result['message'] = ApiWrapper::$setMessage;
        }

        if (is_array(ApiWrapper::$setData) && count(ApiWrapper::$setData) > 0) {
            $result['data'] = ApiWrapper::$setData;
        } else if (is_object(ApiWrapper::$setData)) {
            $result['data'] = ApiWrapper::$setData;
        }

        if (is_array(ApiWrapper::$setErrors) && count(ApiWrapper::$setErrors) > 0) {
            $result['errors'] = ApiWrapper::$setErrors;
        } else if (is_object(ApiWrapper::$setErrors)) {
            $result['errors'] = ApiWrapper::$setErrors;
        }

        $allowedHeaders = [
            "Access-Control-Allow-Origin"   => "*",
            "Access-Control-Allow-Methods"  => "GET, POST, DELETE, PUT, FETCH, OPTIONS, HEAD",
            "Access-Control-Allow-Headers"  => "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization,Origin,Referer"
        ];

        return response()->json($result, $result['status'], $allowedHeaders);
    }

    /**
     * @param int $status
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    protected static function setFormatErrorMessage(int $status, string $message): JsonResponse
    {
        ApiWrapper::$setStatus = $status;
        ApiWrapper::$setMessage = $message;

        return ApiWrapper::getResponse();
    }

    /**
     * @param string $message
     * @param array $data
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseCreated(string $message, $data): JsonResponse
    {
        ApiWrapper::$setStatus = 201;
        ApiWrapper::$setMessage = $message;
        ApiWrapper::$setData = $data;

        return ApiWrapper::getResponse();
    }

    /**
     * @param string $message
     * @param array $data
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseOk(string $message, $data): JsonResponse
    {
        ApiWrapper::$setStatus = 200;
        ApiWrapper::$setMessage = $message;
        ApiWrapper::$setData = $data;

        return ApiWrapper::getResponse();
    }

    /**
     * @param string $message
     * @param int $status
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseError(string $message, $status): JsonResponse
    {
        $status = (int) $status;
        ApiWrapper::$setStatus = $status < 200 || $status > 500 ? 500 : $status;
        ApiWrapper::$setMessage = $message;

        return ApiWrapper::getResponse();
    }

    /**
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseUnauthorized(string $message): JsonResponse
    {
        return ApiWrapper::setFormatErrorMessage(401, $message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseForbidden(string $message): JsonResponse
    {
        return ApiWrapper::setFormatErrorMessage(403, $message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseUnprocessableEntity(string $message): JsonResponse
    {
        return ApiWrapper::setFormatErrorMessage(422, $message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseNotFound(string $message): JsonResponse
    {
        return ApiWrapper::setFormatErrorMessage(404, $message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     * @throws Exception
     */
    public static function responseServerError(string $message): JsonResponse
    {
        return ApiWrapper::setFormatErrorMessage(500, $message);
    }
}
