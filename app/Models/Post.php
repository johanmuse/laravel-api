<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'content',
    ];

    protected $dates = ['published_at'];


 
}
