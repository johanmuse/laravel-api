<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Validator;

class PostController extends Controller
{
    public function index() {
        $data = Post::latest()->get();
        return response()->json($data);
    }
    public function show($id) {
        $check = Post::find($id);
        if(!$check){
            return response()->json(['errors' => 'Post not found']);
        }
        return response()->json(Post::find($id), 201);
    }
    public function store(Request $request) {

        $messages = [
            'title.required' => 'The Reason field is required.',
            'content.required' => 'The Content field is required.',
        ];

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',

        ], $messages);
        
        if($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $post = Post::create($request->all());

        return response()->json(Post::find($post->id), 201);
    }
    public function update($id, Request $request) {

        $messages = [
            'title.required' => 'The Reason field is required.',
            'content.required' => 'The Content field is required.',
        ];

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',

        ], $messages);
        
        if($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $check = Post::find($id);
        if(!$check){
            return response()->json(['errors' => 'Post not found']);
        }
        $post = Post::where('id', $id)->update($request->all());
        return response()->json(Post::where('id', $id)->first());
    }
    public function delete($id) {
        $check = Post::find($id);
        if(!$check){
            return response()->json(['errors' => 'Post not found']);
        }
        $check->delete();
        return response()->json('Post deleted successfully');
    }
}
